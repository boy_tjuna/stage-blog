<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
class Blog extends CI_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('blog_model', 'user_model'));
		$this->load->helper(array('url_helper','form','text'));
		$this->load->library(array('session','form_validation','parsedown'));
		$this->load->database();
	}

	public function index()
	{
		//See if user is admin
		$data['admin'] = $this->session->userdata('user_group_id');
		//owner post check
		$data['owner'] = $this->session->userdata('user_id');
		
		$this->form_validation->set_rules('search', 'Search', 'max_length[30]');

		if ($this->form_validation->run() == false)
		{
			//blog list
			$data['blogs'] = $this->blog_model->get_blogs();
	        $this->load->view('templates/header', $data);
	        $this->load->view('pages/blog_view');
	        $this->load->view('templates/footer');
	    }

	    else 
	    {
	    	$data['blogs'] = $this->blog_model->_search_blog();
	    	$this->load->view('templates/header', $data);
	        $this->load->view('pages/blog_view');
	        $this->load->view('templates/footer');
	    }
	}

	public function add_blog()
	{
		if ($this->user_model->_check_permission(5) == 'create_content') 
		{
			$this->blog_model->blog_validation();

			if ($this->form_validation->run() == false)
			{
				$this->load->view('templates/header');
				$this->load->view('pages/add_blog');
				$this->load->view('templates/footer');
			}

			else
			{
				$title = $this->input->post('title');
				$autor = $this->session->userdata('username');
				$text = $this->input->post('text');
				$category = $this->input->post('category');
				$publish = $this->input->post('publish');

				$this->blog_model->add_blog($title, $autor, $text, $category, $publish);
				redirect('blog/index', 'refresh');
			}
		}

		else
		{
			$this->no_entery();
		}
	}

	public function detail_blog()
	{
			//See if user is admin
			$blog['admin'] = $this->session->userdata('user_group_id');
			//owner post check
			$blog['owner'] = $this->session->userdata('user_id');

			$blog['data'] = $this->blog_model->show_blog()->result();

			$this->load->view('templates/header', $blog);
			$this->load->view('pages/detail_blog');
			$this->load->view('templates/footer');
	}


	public function update_blog()
	{
		$this->blog_model->blog_validation();
		
		if ($this->form_validation->run() == false)
		{
			$owner = $this->blog_model->show_blog()->row();

			if ($this->user_model->_check_permission(6) == 'update_content' || $owner->user_id == $this->session->userdata('user_id')) 
			{
				$blog['data'] = $this->blog_model->show_blog()->result();

				$this->load->view('templates/header', $blog);
				$this->load->view('pages/edit_blog');
				$this->load->view('templates/footer');
			}

			else
			{
				$this->no_entery();
			}
		}

		else
		{
			$blog_id = $this->input->post('blog_id');
			$image_id = $this->input->post('blog_image_id');
			$title = $this->input->post('title');
			$autor = $this->session->userdata('username');
			$text = $this->input->post('text');
			$category = $this->input->post('category');
			$publish = $this->input->post('publish');

			$this->blog_model->update_blog($blog_id, $image_id, $title, $autor, $text, $category, $publish);
			redirect('blog/detail_blog/'.$this->input->post('blog_id'), 'refresh');
		}
		
	}

	public function delete_blog()
	{
		$owner = $this->blog_model->show_blog()->row();

		if ($this->user_model->_check_permission(7) == 'delete_content' || $owner->user_id == $this->session->userdata('user_id')) 
		{
			$this->blog_model->delete_blog();
			redirect('blog/index');
		}
		else
		{
			$this->no_entery();
		}
	}

	public function no_entery()
	{
		echo "You don't have premission to access to this page. ".anchor('blog/index', 'Go back and switch user');
		die();
	}
}