<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Login extends CI_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('user_model');
		$this->load->helper(array('form','url_helper'));
		$this->load->library(array('form_validation', 'session'));
	}

	public function index()
	{
		if (!isset($status) || $status != 1)
		{
			//redirect them to the login page
			redirect('login/login_vertify', 'refresh');
		}

		else
		{
			//logged in user
			redirect('blogs/index', 'refresh');
		}
	}

	public function login_vertify()
	{
		$this->form_validation->set_rules('email','Email','required|valid_email');
		$this->form_validation->set_rules('password','Password','required');

		if ($this->form_validation->run() == false)
		{
	        $this->load->view('templates/header');
	        $this->load->view('pages/login_view');
	        $this->load->view('templates/footer');

	        $this->session->unset_userdata('message');
		}

		else
		{
			$this->user_model->_validate_login();
		}
   }

   public function register()
   {
   		$this->form_validation->set_rules('username','Username','required|min_length[2]|max_length[50]');
   		$this->form_validation->set_rules('email','email','required|valid_email|min_length[4]|max_length[50]');
		$this->form_validation->set_rules('password','password','required|min_length[4]|max_length[100]');
		$this->form_validation->set_rules('password_confirm','password confirm','required|matches[password]');
		$this->form_validation->set_rules('userfile','Userfile','');

		if ($this->form_validation->run() == false)
		{
	        $this->load->view('templates/header');
	        $this->load->view('pages/register_view');
	        $this->load->view('templates/footer');
		}

		else
		{
			if($query = $this->user_model->create_user())
			{
				$this->load->view('templates/header');
		        $this->load->view('pages/activate_account');
		        $this->load->view('templates/footer');
			}

			else
			{
				$this->session->set_flashdata('message', 'The e-mail is already taken.');

				$this->load->view('templates/header');
		        $this->load->view('pages/register_view');
		        $this->load->view('templates/footer');
			}
		}
   }

   public function register_comfirm()
   {
   		$activation_code = $this->uri->segment(3);

   		if ($activation_code == null) {
   		   	echo "no registraion code in URL ".anchor('blog/index', 'Go back');
			die();	
   		}

   		else
   		{
   			$this->user_model->_check_activation_code($activation_code);
   		}

   }

   public function logout()
   {
   		$this->user_model->logout();
   }
}
