<?php 
	foreach ($data as $blog):

		if($blog->publish == 'on' || $owner == $blog->user_id || $admin == 1)
		{ 

?>
		<article class="post">
			<div class="background">
        
		        <div class="top-container">
		          <h1 class="animated fadeInUp delay-3"><?php echo $blog->title;?></h1>
		          <h2 class="tag"><a href="#" class="waves-effect waves-light"><?php echo $blog->category;?></a></h2>
		              
		        	<div class="avatar animated slideInDown delay-3"><img src="<?php echo base_url();?>static/img/<?php echo $blog->autor_image;?>"></div>
		        	</div>
			        <svg class="line-container" width="100%" viewBox="0 50 568 10">
			          <polygon class="line" points="568,568 0,568 0,86 568,0 "/>
			        </svg>
			        
			        <div class="info-bar">
			            <div class="info-item date animated fadeInDown delay-4">
			              <h4>author</h4>
			              <h4 class="subtitle"><?php echo $blog->autor;?></h4>
			            </div>
			            <div class="info-item time animated fadeInDown delay-4">
			              <h4>date</h4>
			              <h4 class="subtitle"><?php echo $blog->date = date("F jS");?></h4>
			            </div>
			        </div>
		    	</div>
	
				<div class="content">

		          	<div class="description animated slideInUp delay-4">
		            	<p><?php echo preg_replace("/[\r\n]/","<p>", $blog->text);?></p>
		        	</div>

			   <?php 	
				   	if($owner == $blog->user_id || $admin == 1)
					{ 
		        		echo anchor('blog/delete_blog/'.$blog->blog_id, 'Delete', 'class="link"');
		        		echo "<br>";
						echo anchor('blog/update_blog/'.$blog->blog_id, 'Edit', 'class="link"');
					}
				?>
		        </div>
	      	</div>
	    </article>
		
	<style type="text/css">

	.container .background {
	  background: url(<?php echo base_url();?>static/img/<?php echo $blog->blog_image;?>);
	  position: relative;
	  height: 380px;
	  background-size: cover;
	  background-repeat: no-repeat;
	  background-position: center;
	  clear: both;
	}

	</style>	
<?php	
		}

 	endforeach; 
 ?>