<main>
  
  <article class="post white-block">
    <header>
        <h1>Create a new post</h1>
    </header>

    <p>Please fill in the form below to complete your registration.</p>

    <?php echo form_open_multipart('blog/add_blog'); ?>
      <ul>
        <li><input type="text" name="title" placeholder="Title" required></li>
        <li><input type="text" name="category" placeholder="Category" required></li>
        <li><textarea name="text" placeholder="Your story" cols="30" rows="10"></textarea></li>
        <li><input type="checkbox" name="publish" class="filled-in" id="filled-in-box"><label for="filled-in-box">Publish?</label></li>
        <li><label for="image" class="img-selector"><i class="material-icons">add_a_photo</i><input type="file" name="userfile"></li></label>
      </ul>
          <button type="submit" value="Login" class="waves-effect waves-light btn"><i class="material-icons left">lock</i>Submit</button>
    <?php echo form_close();?>
    
  </article>

</main>