<main>
  
  <article class="post white-block">
    <header>
        <h1>Edit post</h1>
    </header>

    <p>Please fill in the form below to complete your registration.</p>
    <?php foreach ($data as $blog): ?>
    <?php echo form_open_multipart('blog/update_blog'); ?>
      <ul>
          <li><input type="hidden" name="blog_id" value="<?php echo $blog->blog_id;?>"></li>
          <li><input type="hidden" name="blog_image_id" value="<?php echo $blog->blog_image_id;?>"></li>
          <li><input type="text" name="title" placeholder="Title" value="<?php echo $blog->title;?>" required></li>
          <li><textarea name="text" placeholder="Text" required><?php echo $blog->text;?></textarea> </li>
          <li><input type="text" name="category" placeholder="Category" value="<?php echo $blog->category;?>" required></li>
          <li><input type="checkbox" name="publish" class="filled-in" id="filled-in-box"><label for="filled-in-box">Publish?</label></li>
          <li><label for="image" class="img-selector"><i class="material-icons">add_a_photo</i><input type="file" name="userfile"></li></label>
      </ul>
        <button type="submit" value="Login" class="waves-effect waves-light btn"><i class="material-icons left">lock</i>Edit</button>
    <?php echo form_close();?>
    
  </article>
  <?php endforeach; ?>

</main>