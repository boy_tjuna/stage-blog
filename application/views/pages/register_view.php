<main>
  
  <article class="post white-block">
    <header>
        <h1>Register</h1>
    </header>

    <p>Please fill in the form below to complete your registration.</p>

    <?php echo validation_errors('<div class="toast">', '</div>'); ?>
    <?php if($this->session->flashdata('message')){echo '<div class="toast">'.$this->session->flashdata('message').'</div>';}?>
    <?php echo form_open_multipart('login/register'); ?>
      <ul>
          <li><input type="text" name="username" placeholder="Name" required></li>
          <li><input type="email" name="email" placeholder="Email" required></li>
          <li><input type="password" name="password" placeholder="password" required></li>
          <li><input type="password" name="password_confirm" placeholder="password confirm" required></li>
          <li><label for="avatar">Avatar: <input type="file" name="userfile"></li></label>
          <button type="submit" value="Login" class="waves-effect waves-light btn"><i class="material-icons left">person_add</i>Register</button><?php echo anchor('login/login_vertify', 'Log in', 'class="link"'); ?>
      </ul>
    <?php echo form_close();?>
    
  </article>

</main>