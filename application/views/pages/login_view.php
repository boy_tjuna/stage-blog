<main>
        
  <article class="post white-block">
    <header>
        <h1>Login</h1>
    </header>

    <?php if($this->session->userdata('message')){echo '<div class="toast">'.$this->session->userdata('message').'</div>';}?>
    <?php echo form_open('login/login_vertify'); ?>
      <ul>
          <li><input type="email" name="email" placeholder="Email" required></li>
          <li><input type="password" name="password" placeholder="password" required></li>
          <button type="submit" value="Login" class="waves-effect waves-light btn"><i class="material-icons left">lock</i>Login</button>
          <?php echo anchor('login/register', 'Create new account', 'class="link"'); ?>
      </ul>
    <?php echo form_close();?>
    
  </article>

</main>