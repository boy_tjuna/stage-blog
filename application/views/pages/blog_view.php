<main>
<?php 
	foreach ($blogs as $blog):

		if($admin == 1)
		{
;?>
			<article class="post white-block">
	          	<header>
					<h1><?php echo anchor('blog/detail_blog/'.$blog->blog_id, $blog->title);?></h1>
					<span class="time"><?php echo $blog->date = date("H:i a \- F jS");?></span> 
				</header>

				<p>
					<?php echo $blog->text = word_limiter($blog->text, 50);?> 
				</p>

				<?php if (str_word_count($blog->text) > 50) {
				?>

				<div class="read-more">
            		<span><?php echo anchor('blog/detail_blog/'.$blog->blog_id, '-read more-');?></span>
            	</div>

            	<?php } ;?>
			</article>

<?php 
		}

		elseif($blog->publish == 'on' || $owner == $blog->user_id)
		{ 
?>
	<div class="grid-item">
		<article class="post white-block">
          	<header>
				<h1><?php echo anchor('blog/detail_blog/'.$blog->blog_id, $blog->title);?></h1>
				<span class="time"><?php echo $blog->date = date("H:i a \- F jS");?></span> 
			</header>

			<p>
				<?php echo $blog->text = word_limiter($blog->text, 50);?> 
			</p>
			
			<?php if (str_word_count($blog->text) > 50) {
			?>

			<div class="read-more">
        		<span><?php echo anchor('blog/detail_blog/'.$blog->blog_id, '-read more-');?></span>
        	</div>

        	<?php } ;?>
		</article>
	</div>
			
<?php	
		}

 	endforeach; 
 ?>
 </main>