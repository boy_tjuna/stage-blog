<!DOCTYPE html>
<html class="no-js" lang="nl">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<title> Boy Wu </title>
		<meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

         <link rel="apple-touch-icon" href="<?php echo base_url();?>/about_me.png">
         <link rel="shortcut icon" type="<?php echo base_url();?>/image/png" href="<?php echo base_url();?>img/about_me.png">

        <link href='https://fonts.googleapis.com/css?family=Istok+Web:400,700,700italic|Lora:400,700' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="<?php echo base_url();?>css/style.css" type='text/css'>
		<link rel="stylesheet" href="<?php echo base_url();?>css/normalize.css" type='text/css'>
        <link rel="stylesheet" href="<?php echo base_url();?>css/main.css" type='text/css'>
        <script src="<?php echo base_url();?>js/modernizr-2.8.3.min.js"></script>
	</head>

	<body>
	
		<nav class="side_menu">
			<ul>
				<li> 
					<a href="#home">
						<div class="circle">
							<img src="<?php echo base_url();?>img/Home.png" class="menu_icon">
						</div>
					</a> 
					<ul>
						<li>
							<a href="#home">
								<div class="square">Home</div>
							</a>
						</li>
					</ul>
				</li>
				<li> 
					<a href="#about_me">
						<div class="circle">
							<img src="<?php echo base_url();?>img/about_me.png" class="menu_icon">
						</div>
					</a> 
					<ul>
						<li>
							<a href="#about_me">
								<div class="square">Over mij</div>
							</a>
						</li>
					</ul>
				</li>
				<li> 
					<a href="#portfolio">
						<div class="circle">
							<img src="<?php echo base_url();?>img/Portfolio.png" class="menu_icon">
						</div>
					</a> 
					<ul>
						<li>
							<a href="#portfolio">
								<div class="square">portfolio</div>
							</a>
						</li>
					</ul>
				</li>
				<li> 
					<a href="#contact">
						<div class="circle">
							<img src="<?php echo base_url();?>img/Contact.png" class="menu_icon">
						</div>
					</a> 
					<ul>
						<li>
							<a href="#contact">
								<div class="square">contact</div>
							</a>
						</li>
					</ul>
				</li>
			</ul>
		</nav>

		<div class="sticky_footer">
			
			<header id="home">
				<h1 class="title">Boy Wu</h1>
				<h3 class="subtitle">Intern Developer</h3>
				<h4><a href="#about_me" class="btn">Learn more about me</a></h4>
			</header>

			<article class="container">

				<nav class="top_menu">
					<ul>
						<li>
							<a href="#home" class="home_logo">
							</a>
						</li>
						<li>
							<a href="#about_me">Over mij</a>
						</li>
						<li>
							<a href="#portfolio">Portfolio</a>
						</li>
						<li>
							<a href="#contact">Contact</a>
						</li>
					</ul>
				</nav>

				<div class="content">
					
					<h1 id="about_me"> Over mij </h1>

						<h2> Wie ben ik? </h2>

							<p>
								Mijn naam is Boy Wu. ik ben 20 jaar oud. ik ben geboren en getogen in IJmuiden.
							</p>

						<h2> Opleiding </h2>

							<p>
								Op dit moment zit ik op het Hogeschool InHolland in Haarlem. Hier doe ik de opleiding Informatica. Het is momenteel mijn 3e jaar. Op de opleiding zelf het ik verschillende programeertalen en technieken geleerd. De programeertalen die ik ben zijn:

							</p>

							<ol class="list" >
								<li>HTML5</li>
								<li>CSS3</li>
								<li>C#</li>
								<li>Java</li>
								<li>PHP</li>
								<li>Javascript</li>
							</ol>

							<p>
								Naast deze programeertalen heb ik ook allerdaagse technieken geleerd die men in het bedrijfleven gebruikt als:
							</p>

							<ul class="list">
								<li>Scrum</li>
								<li>UML</li>
							</ul>

						<h2> Hobbies </h2>

							<p>
								Mijn hobbies zijn gamen, series kijken en tekenen.
							</p>

						<h2> Sport </h2>

							<p>
								Momenteel zit ik alleen op karate. Dit doe ik al bijna 7 jaar. Ik heb een bruine band en ik ben het minimaal nog van plan te doen tot mijn zwarte band.
							</p>

							<p>
								Voorheen heb ik 2 jaar op voetbal gezeten met een vriendenteam. De tijd met het team was heel gezellig, maar we zijn uiteindelijk gestopt.
								Ten slotte heb ik in mijn jeugd nog aan schaken gedaan. Dit heb ik 5 jaar gedaan. 
							</p>

					<h1 id="portfolio"> Portfolio </h1>

						<div class="column">

							<p class="bold">
								Lorem Khaled Ipsum is a major key to success. It’s on you how you want to live your life. Everyone has a choice. I pick my choice, squeaky clean. Put it this way, it took me twenty five years to get these plants, twenty five years of blood sweat and tears, and I’m never giving up, I’m just getting started. Fan luv. Another one. Congratulations, you played yourself. Stay focused. The key to more success is to get a massage once a week, very important, major key, cloth talk. Put it this way, it took me twenty five years to get these plants, twenty five years of blood sweat and tears, and I’m never giving up, I’m just getting started.
							</p>

							<iframe src="http://www.youtube.com/embed/D9g2szHsoz0">
							</iframe>

							<p>
								Congratulations, you played yourself. Lion! Lion! Always remember in the jungle there’s a lot of they in there, after you overcome they, you will make it to paradise. The key to more success is to have a lot of pillows. To be successful you’ve got to work hard, to make history, simple, you’ve got to make it. You should never complain, complaining is a weak emotion, you got life, we breathing, we blessed. In life there will be road blocks but we will over come it.
							</p>

						</div>

						<div class="column">

							<p class="italic">
								<bdo dir="rtl">
									In life there will be road blocks but we will over come it. Celebrate success right, the only way, apple. It’s on you how you want to live your life. Everyone has a choice. I pick my choice, squeaky clean. In life there will be road blocks but we will over come it. Eliptical talk. You see that bamboo behind me though, you see that bamboo? Ain’t nothin’ like bamboo. Bless up. Surround yourself with angels, positive energy, beautiful people, beautiful souls, clean heart, angel. It’s important to use cocoa butter. It’s the key to more success, why not live smooth? Why live rough?
								</bdo>
							</p>

							<img src="<?php echo base_url();?>img/blabla.jpg">

						</div>

						<div class="clearfix"></div>

					<h1 id="contact"> Contact </h1>
					
						<p>
							Neem gerust contact met mij op in het contactformulier. Als je het liever mijn e-mail wil: boy@tjuna.com
						</p>

						<form action="MAILTO:boy@tjuna.com" method="post" enctype="text/plain" id="contact_form">
							<br>
							<p>Comment:</p>
							<textarea name=" " class="inputs" form="contact_form" rows="10" cols="40"></textarea>
							<br>

							<input type="submit" value="Send" class="btn">
							<input type="reset" value="Reset" class="btn">
						</form>

					</div>
				
			</article>

			<div class="push"></div>

		</div>

		<footer>
				<ul class="left"> 
					<li>
						<a href="<?php echo base_url();?>#home">Home</a>
					</li>
					<li>
						<a href="<?php echo base_url();?>#about_me">Over mij</a>
					</li>
				</ul>

				<ul class="center">
					<li>
						<a href="<?php echo base_url();?>#portfolio">Portfolio</a>
					</li>
					<li>
						<a href="<?php echo base_url();?>#contact">Contact</a>
					</li>
					<li>
						<?php echo anchor('blog/index', 'Blog');?>
					</li>
				</ul>

			    <ul class="right">
					<li>
						<a href="http://www.facbook.com"><img src="<?php echo base_url();?><?php echo base_url();?>img/facebook.png" class="social_media"></a>
						<a href="http://www.linkedin.com"><img src="<?php echo base_url();?><?php echo base_url();?>img/linkedin.png" class="social_media"></a>
						copyright &#169 2016
					</li>
				</ul>
		
			</footer>

	</body>

</html>
