<!DOCTYPE html>
<html>
  <head>
    <!--Import Google Icon Font-->
    <title>Blog Home</title>
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Merriweather:400,400italic,700|Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link type="text/css" rel="stylesheet" href="<?php echo base_url();?>static/css/style.css"  media="screen,projection"/>
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  </head>

  <body>
    <div class="nav-overlay">
      <i id="menu-close" class="material-icons close">close</i>
        <div class="inner-overlay valign-wrapper">
            <ul class="valign">
              <li><?php echo anchor('blog/index', 'home'); ?></li>
              <li><?php echo anchor('login/login_vertify', 'login'); ?></li>
              <li><?php echo anchor('login/register', 'register'); ?></li>
            </ul>
        </div>
    </div>   
  <div class="container">

    <header class="site-header">
      <?php $status = $this->session->userdata('status');
            if (isset($status) && $status == 1) 
            { ?>
              <div class="logged-in-bar">
                
              <div class="dropdown">
              <i id="submenu-toggle" class="material-icons">keyboard_arrow_down</i>
                <ul class="nav-submenu">
                  <li><?php echo anchor('blog/add_blog', 'New post')?></li>
                  <li><?php echo anchor('login/logout', 'Logout')?></li>
                </ul>
              </div>
                
                <span class="name"><?php echo $this->session->userdata('username');?></span>
                
                <img src="<?php echo base_url();?>static/img/<?php echo $this->session->userdata('profile_pic') ?>" alt="Avatar" class="top-avatar">
              </div>
      <?php };?>
      
      <div class="site-titles">
      <a href="<?= base_url() ?>">
        <h1 class="site-title">This is a blogtitle</h1>
        <h2 class="site-subtitle">I am Singing Wind, Chief of the Martians.</h2>
        </a>
      </div>

      <div class="menu-bar">

        <?php if (!isset($status) || $status == null || $status == 0) 
              { ?>
                <i id="menu-open" class="material-icons hamburger">menu</i>
        <?php }; ?>
        <?php 
          echo form_open('blog/index'); 
        ?>

      <div class="search-form">
      <i class="material-icons search-icon">search</i>
          <input type="submit" name="submit" class="submit-button" value="&nbsp;">
          <input id="search" name="search" type="search">
      </div>
      
      <?php echo form_close();?> 
      
    </div>
  </header>
