<?php
Class User_model extends CI_model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('image_model');
    }

	public function _validate_login()
	{	
		$query = $this->db->select('user.*, image.image_name')
						->from('user')
						->join('image', 'image.image_id = user.image_id', 'inner')
						->where('email', $this->input->post('email'))
						->where('Password', Sha1($this->input->post('password')))
						->get();
		//if username/password is correct
		if ($query->num_rows() == 1) 
		{

			$user = $query->row();
			if($user->activated == 1) 
			{
				$data = array(
					'user_id' => $user->user_id,
					'username' => $user->username,
					'email' => $user->email,
					'profile_pic' => $user->image_name,
					'status' => $user->status,
					'user_group_id' => $user->user_group_id
				);

				$this->session->set_userdata($data);
				redirect('blog/index');
			}

			else
			{
				$message = array('message' => 'You have not activated your account yet');
				$this->session->set_userdata($message);
				redirect('login/login_vertify');
			}
		}
		//if incorrect
		else
		{
			$message = array('message' => 'Your username/password is incorrect');
			$this->session->set_userdata($message);
			redirect('login/login_vertify');
		}
	}

	public function create_user()
	{
		$query = $this->db->select('user.*')
						->from('user')
						->where('email', $this->input->post('email'))
						->get();

		//if username is not taken
		if ($query->num_rows() == 0) 
		{
			$activation_code = $this->_random_string_generator(10);

			//upload image if exist
	        $size = 'thumbnail';

	        if ($this->image_model->do_upload($size)) 
	        {
	            $image_id = $this->db->select('image_id')
	                            ->order_by('image_id','desc')
	                            ->limit(1)->get('image')
	                            ->row('image_id');
	        } 

	        else 
	        {
	            //default foto
	            $image_id = 1;
	        }

			$new_user_data = array(	
			'username' => $this->input->post('username'),
			'email' => $this->input->post('email'),
			'password' => sha1($this->input->post('password')),
			'image_id' => $image_id,
			'activation_code' => $activation_code
			);

			$insert = $this->db->insert('user', $new_user_data);

			//send registration e-mail
			$config = Array(
			    'mailtype' => 'html'
			);

			$this->load->library('email');
			$this->email->initialize($config);

			$this->email->from('boy@tjuna.com', 'Boy');
			$this->email->to($this->input->post('email'));
			$this->email->subject('Registration Confirm');
			$this->email->message('Please click this link to confirm your registration. '.anchor('login/register_comfirm/'.$activation_code, 'Confirm registration'));
			$this->email->send();
			
			return true;
		}
		//if taken
		else
		{
			return false;
		}

	}

	public function logout()
   	{
   		$this->session->unset_userdata('user_id');
   		$this->session->unset_userdata('username');
   		$this->session->unset_userdata('status');
   		$this->session->unset_userdata('image_id');
   		$this->session->unset_userdata('user_group_id');

   		redirect('login/login_vertify', 'refresh');
  	}

  	public function _check_permission($permission_id)
   	{
		$query = $this->db->select('permission.key')
			->from('user_group_to_permission')
			->join('permission', 'permission.permission_id = user_group_to_permission.permission_id', 'inner')
			->where('user_group_to_permission.user_group_id', $this->session->userdata('user_group_id'))
			->where('user_group_to_permission.permission_id', $permission_id)
			->get();


		//if user has premission
		if ($query->num_rows() > 0) {

			$user = $query->row();

			return $user->key;
		}
		//if not
		else
		{
			return false;
		}
   	}

   	function _random_string_generator($length) 
	{
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';

	    for ($i = 0; $i < $length; $i++) 
	    {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }

	    return $randomString;
	}

	public function _check_activation_code($activation_code)
	{
		$query = $this->db->select('user.activation_code, user.user_id')
						->from('user')
						->where('user.activation_code', $activation_code)
						->get();

		if ($query->num_rows() == 1)
		{
			$user = $query->row();

			$data = array('activated' => 1);

			$this->db->where('user_id', $user->user_id);
        	$this->db->update('user', $data);

        	$this->load->view('templates/header');
	        $this->load->view('pages/login_successful');
	        $this->load->view('templates/footer');
		}

		else
		{
			$this->load->view('templates/header');
	        $this->load->view('pages/login_unsuccessful');
	        $this->load->view('templates/footer');
		}

	}
}