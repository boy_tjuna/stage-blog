<?php
Class Image_model extends CI_model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->helper(array('form', 'url'));
    }

    public function do_upload($size)
    {
	    $config['upload_path'] = realpath(APPPATH . '../static/img');;
	    $config['allowed_types'] = 'gif|jpg|png';
	    $config['max_size'] = '2048000';
	    $config['max_width']  = '2000';
	    $config['max_height']  = '2000';
	    $config['overwrite'] = TRUE;
   		$config['remove_spaces'] = TRUE;

		$this->load->library('upload', $config);

		if ($this->upload->do_upload())
		{
			$data = array('image_name' => $this->upload->data());

			if($size == 'thumbnail')
			{
				$this->thumb($data);
				$file = array(
					'image_name'=> $this->upload->data('raw_name').'_thumb'.$this->upload->data('file_ext')
				);
			}

			else
			{
				$file = array(
					'image_name'=> $this->upload->data('file_name')
				);
			}

			$this->db->insert('image', $file);

			return true;
    	}

    	else
    	{
   //  		echo $this->upload->display_errors();
			
			// $this->load->view('refresh', $error);

			return false;
    	}
    }

    function thumb($data)
	{
		$config['image_library'] = 'gd2';
		$config['source_image'] = $this->upload->data('full_path');
		$config['create_thumb'] = TRUE;
		$config['maintain_ratio'] = TRUE;
		$config['width'] = 200;
		$config['height'] = 200;
		$this->load->library('image_lib', $config);
		$this->image_lib->resize();
	}
}