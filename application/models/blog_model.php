<?php
class Blog_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function get_blogs()
    {
        $query = $this->db->order_by('date', 'desc')
        ->get('blog');
        return $query->result();
    }

    public function add_blog($title, $autor, $text, $category, $publish)
    {
        //If not checked
        if (!isset($publish) || $publish == null) {
            $publish = 'off';
        }

        //upload image if exist
        $size = 'normal';

        if ($this->image_model->do_upload($size)) 
        {
            $image_id = $this->db->select('image_id')
                            ->order_by('image_id','desc')
                            ->limit(1)->get('image')
                            ->row('image_id');
        } 

        else 
        {
            //default foto
            $image_id = 2;
        }

        $data = array(
            'user_id' => $this->session->userdata('user_id'),
            'title' => $title,
            'autor' => $autor,
            'text' => $text,
            'category' => $category,
            'publish' => $publish,
            'image_id' => $image_id
        );

        $this->db->insert('blog', $data);
    }

    public function show_blog()
    {
        $query = $this->db->select('blog.*, image_user.image_name autor_image, image_blog.image_name blog_image, image_blog.image_id blog_image_id')
                        ->from('blog')
                        ->join('user', 'user.user_id = blog.user_id', 'inner')
                        ->join('image image_blog', 'image_blog.image_id = blog.image_id', 'right')
                        ->join('image image_user', 'image_user.image_id = user.image_id', 'right')
                        ->where('blog_id', $this->uri->segment(3))
                        ->get();
        return $query;
    }

    public function update_blog($blog_id, $image_id, $title, $autor, $text, $category, $publish)
    {

        if ($this->image_model->do_upload($size = 'normal')) 
        {
            $image_id = $this->db->select('image_id')
                            ->order_by('image_id','desc')
                            ->limit(1)->get('image')
                            ->row('image_id');
        } 

        else 
        {
            //default foto
            $image_id = 2;
        }

        $data = array(
            'user_id' => $this->session->userdata('user_id'),
            'title' => $title,
            'date' => null,
            'autor' => $autor,
            'text' => $text,
            'category' => $category,
            'publish' => $publish,
            'image_id' => $image_id
        );

        $this->db->where('blog_id', $blog_id);
        $this->db->update('blog', $data);
    }

    public function delete_blog()
    {
        $this->db->where('blog_id', $this->uri->segment(3));
        $this->db->delete('blog');
    }

    public function blog_validation()
    {   
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        $this->form_validation->set_rules('title', 'Title', 'required|max_length[100]');
        $this->form_validation->set_rules('text', 'Text', 'required');
        $this->form_validation->set_rules('category', 'Category', 'required');
        $this->form_validation->set_rules('publish', 'Publish', '');
        $this->form_validation->set_rules('userfile', 'Userfile', '');
    }

    public function _search_blog()
    {
        $query = $this->db->select('blog.*')
                        ->from('blog')
                        ->like('blog.title', $this->input->post('search'))
                        ->or_like('blog.text', $this->input->post('search'))
                        ->or_like('blog.autor', $this->input->post('search'))
                        ->or_like('blog.category', $this->input->post('search'))
                        ->get();
        return $query->result();
    }
}