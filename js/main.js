$('.post:eq(0)').addClass('animated fadeInUp delay-1');
$('.post:eq(1)').addClass('animated fadeInUp delay-3');
$('.post:eq(2)').addClass('animated fadeInUp delay-4');
$('.post:eq(3)').addClass('animated fadeInUp delay-4');

$('li:eq(0)').addClass('animated slideInUp delay-1');
$('li:eq(1)').addClass('animated slideInUp delay-3');
$('li:eq(2)').addClass('animated slideInUp delay-4');

$('.toast').click(function() {
	$('.toast').fadeOut();
});

$('#menu-open').click(function() {
	$('.nav-overlay').slideDown();
});

$('#menu-close').click(function() {
	$('.nav-overlay').slideUp();
});

$('.search-icon').click(function() {
	if ($('#search').is(':active')) {
		$('.search-icon').addClass('active');
	} else {
		$('#search').trigger( "focus" );
		$('.search-icon').addClass('active');
	}
});

// $(".search-icon").click(function () {
//   if ( $( "#search" ).is(':active')) {
//     $( ".search-icon" ).css({'z-index': '-1'});
//   } else {
//     $( ".search-icon" ).css({'z-index': '999'});
//     $( ".search-icon" ).css({'z-index': '999'});
//   }
// });

// function print(message) {
//   document.write('<h1>' + message + '</h1>');
// }

// var questions = [
// 	['question1', 'a1'],
// 	['question2', 'a2'],
// 	['question3', 'a3'],
// 	['question4', 'a4'],
// 	['question5', 'a5']
// ];

// var correctQuestions = 0;
// var wrongQuestions	 = 0;

// for (var i = 0; i < questions.length; i++) {
// 	var userAnswer = prompt('The current question is: ' + questions[i][0]);

// 	if (userAnswer.toLowerCase() === questions[i][1]) {
// 		correctQuestions++;
// 	} else {
// 		wrongQuestions++;
// 	}

// }

// print("You got " + correctQuestions + " question(s) right, and " + wrongQuestions + " question(s) wrong.");

